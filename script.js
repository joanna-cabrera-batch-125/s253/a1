db.users.insertMany(
	[
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"Email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"Email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firelli",
			"Email": "jfirelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"Email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"Email": "pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"Email": "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
		]

);

db.users.find()

db.courses.insertMany(
	[
		{
			"Name": "Professional Development",
			"Price": "10000"
		},
		{
			"Name": "Business Processing",
			"Price": "13000"
		}
	]
);

db.courses.find()

db.users.find(
		{
			$or: [
				{"firstName": "George"},
				{"isActive": false}
			]
		}
	);

db.users.find( {},
	
	{_id: 0, firstName: 1, lastName: 1}
	)

db.users.find(
		{
			$and: [
				{"isAdmin": true},
				{"isActive": true}
			]
		}
	);

db.user.find(
		{
			"firstName": { $regex: "U", $options: "i"},

		}
	);

db.courses.find(
	{
		{"firstName": { $regex: "U", $options: "i"}}
		{"Price": {$gte: 13000}}
	})